mod error;
mod models;
mod progress_overlay;
mod url_overlay;

use self::error::{ArticleViewError, ArticleViewErrorKind};
pub use self::models::ArticleTheme;
use self::progress_overlay::ProgressOverlay;
use self::url_overlay::UrlOverlay;
use crate::app::{Action, App, WEBKIT_DATA_DIR};
use crate::self_stack::SelfStack;
use crate::settings::Settings;
use crate::util::{constants, DateUtil, GtkUtil, Util, CHANNEL_ERROR, GTK_RESOURCE_FILE_ERROR};
use chrono::{DateTime, Duration, Utc};
use futures::channel::oneshot::Sender as OneShotSender;
use gdk4::{Key, ModifierType, ScrollDirection, ScrollEvent, RGBA};
use gio::Cancellable;
use glib::{clone, source::Continue, MainLoop, SignalHandlerId, SourceId};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, EventControllerKey, EventControllerMotion,
    EventControllerScroll, EventSequenceState, GestureDrag, Inhibit, Overlay, Stack, TickCallbackId, Widget,
};
use javascriptcore::ValueExt;
use log::{error, warn};
use news_flash::models::{Enclosure, FatArticle, Marked, Read};
use pango::FontDescription;
use parking_lot::RwLock;
use rust_embed::RustEmbed;
use std::str;
use std::sync::Arc;
use url::{Host, Origin};
use webkit2gtk::traits::{SettingsExt as WebkitSettingsExt, *};
use webkit2gtk::{
    ContextMenuAction, NavigationPolicyDecision, NetworkProxyMode, NetworkProxySettings, PolicyDecisionType,
    Settings as WebkitSettings, WebContext, WebView, WebsiteDataManagerBuilder, WebsiteDataManagerExtManual,
    WebsiteDataTypes,
};

#[derive(RustEmbed)]
#[folder = "data/resources/article_view"]
struct ArticleViewResources;

//const MIDDLE_MOUSE_BUTTON: u32 = 2;
const SCROLL_TRANSITION_DURATION: i64 = 500 * 1000;

#[derive(Clone, Debug)]
pub struct ScrollAnimationProperties {
    pub start_time: Arc<RwLock<Option<i64>>>,
    pub end_time: Arc<RwLock<Option<i64>>>,
    pub scroll_callback_id: Arc<RwLock<Option<TickCallbackId>>>,
    pub transition_start_value: Arc<RwLock<Option<f64>>>,
    pub transition_diff: Arc<RwLock<Option<f64>>>,
}

mod imp {
    use super::*;
    use glib::subclass;
    use libadwaita::StatusPage;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_view.ui")]
    pub struct ArticleView {
        #[template_child]
        pub progress_overlay: TemplateChild<Overlay>,
        #[template_child]
        pub url_overlay: TemplateChild<Overlay>,

        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub self_stack: TemplateChild<SelfStack>,
        #[template_child]
        pub empty_status: TemplateChild<StatusPage>,

        pub view: RwLock<WebView>,
        pub web_context: RwLock<WebContext>,

        #[template_child]
        pub scroll_event: TemplateChild<EventControllerScroll>,
        #[template_child]
        pub key_event: TemplateChild<EventControllerKey>,
        #[template_child]
        pub motion_event: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub drag_gesture: TemplateChild<GestureDrag>,

        #[template_child]
        pub url_overlay_label: TemplateChild<UrlOverlay>,
        #[template_child]
        pub progress_overlay_label: TemplateChild<ProgressOverlay>,

        pub visible_article: RwLock<Option<FatArticle>>,
        pub visible_feed_name: RwLock<Option<String>>,
        pub visible_article_enclosures: RwLock<Option<Vec<Enclosure>>>,

        pub load_start_timestamp: RwLock<DateTime<Utc>>,

        pub decide_policy_signal: RwLock<Option<SignalHandlerId>>,
        pub mouse_over_signal: RwLock<Option<SignalHandlerId>>,
        pub ctx_menu_signal: RwLock<Option<SignalHandlerId>>,
        pub load_signal: RwLock<Option<SignalHandlerId>>,

        pub drag_ongoing: RwLock<bool>,
        pub drag_buffer: RwLock<[f64; 10]>,
        pub drag_y_offset: RwLock<f64>,
        pub drag_momentum: RwLock<f64>,
        pub pointer_pos: RwLock<(f64, f64)>,
        pub current_scroll_pos: RwLock<f64>,
        pub drag_buffer_update_signal: RwLock<Option<SourceId>>,
        pub drag_released_motion_signal: RwLock<Option<SourceId>>,
        pub scroll_animation_data: ScrollAnimationProperties,
    }

    impl Default for ArticleView {
        fn default() -> Self {
            let mut data_manager_builder = WebsiteDataManagerBuilder::new();
            if let Some(webkit_data_dir) = WEBKIT_DATA_DIR.to_str() {
                data_manager_builder = data_manager_builder
                    .base_cache_directory(webkit_data_dir)
                    .base_data_directory(webkit_data_dir);
            }
            let data_manager = data_manager_builder.build();
            let proxies = App::default().settings().read().get_proxy();
            if !proxies.is_empty() {
                let mut proxy_settings = NetworkProxySettings::new(None, &[]);
                for proxy in proxies {
                    match proxy.protocoll {
                        crate::settings::ProxyProtocoll::ALL => {
                            proxy_settings.add_proxy_for_scheme("http", &proxy.url);
                            proxy_settings.add_proxy_for_scheme("https", &proxy.url);
                        }
                        crate::settings::ProxyProtocoll::HTTP => {
                            proxy_settings.add_proxy_for_scheme("http", &proxy.url)
                        }
                        crate::settings::ProxyProtocoll::HTTPS => {
                            proxy_settings.add_proxy_for_scheme("https", &proxy.url)
                        }
                    }
                }
                data_manager.set_network_proxy_settings(NetworkProxyMode::Custom, Some(&mut proxy_settings));
            }
            let web_context = WebContext::with_website_data_manager(&data_manager);
            web_context.set_cache_model(webkit2gtk::CacheModel::DocumentBrowser);

            Self {
                progress_overlay: TemplateChild::default(),
                url_overlay: TemplateChild::default(),
                stack: TemplateChild::default(),
                self_stack: TemplateChild::default(),
                empty_status: TemplateChild::default(),

                load_start_timestamp: RwLock::new(Utc::now()),

                view: RwLock::new(super::ArticleView::new_webview(&web_context)),
                web_context: RwLock::new(web_context),

                scroll_event: TemplateChild::default(),
                key_event: TemplateChild::default(),
                motion_event: TemplateChild::default(),
                drag_gesture: TemplateChild::default(),

                url_overlay_label: TemplateChild::default(),
                progress_overlay_label: TemplateChild::default(),

                visible_article: RwLock::new(None),
                visible_feed_name: RwLock::new(None),
                visible_article_enclosures: RwLock::new(None),

                decide_policy_signal: RwLock::new(None),
                mouse_over_signal: RwLock::new(None),
                ctx_menu_signal: RwLock::new(None),
                load_signal: RwLock::new(None),

                drag_ongoing: RwLock::new(false),
                drag_buffer: RwLock::new([0.0; 10]),
                drag_y_offset: RwLock::new(0.0),
                drag_momentum: RwLock::new(0.0),
                pointer_pos: RwLock::new((0.0, 0.0)),
                current_scroll_pos: RwLock::new(0.0),
                drag_buffer_update_signal: RwLock::new(None),
                drag_released_motion_signal: RwLock::new(None),
                scroll_animation_data: ScrollAnimationProperties {
                    start_time: Arc::new(RwLock::new(None)),
                    end_time: Arc::new(RwLock::new(None)),
                    scroll_callback_id: Arc::new(RwLock::new(None)),
                    transition_start_value: Arc::new(RwLock::new(None)),
                    transition_diff: Arc::new(RwLock::new(None)),
                },
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleView {
        const NAME: &'static str = "ArticleView";
        type ParentType = Box;
        type Type = super::ArticleView;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleView {}

    impl WidgetImpl for ArticleView {}

    impl BoxImpl for ArticleView {}
}

glib::wrapper! {
    pub struct ArticleView(ObjectSubclass<imp::ArticleView>)
        @extends Widget, Box;
}

impl ArticleView {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let imp = imp::ArticleView::from_instance(self);

        imp.self_stack.set_transition_duration(150);
        imp.self_stack.set_widget(&*imp.view.read());

        self.connect_event_controller();
        self.connect_webview();

        imp.view.read().load_html("", None);
        imp.view.read().set_sensitive(false);
        imp.stack.set_visible_child_name("empty");
    }

    pub fn show_article(&self, article: FatArticle, feed_name: String, enclosures: Option<Vec<Enclosure>>) {
        let imp = imp::ArticleView::from_instance(self);

        Self::stop_scroll_animation(&imp.view.read(), &imp.scroll_animation_data);
        Self::set_scroll_pos_static(&imp.view.read(), 0.0);

        imp.url_overlay_label.reveal(false);
        imp.progress_overlay_label.reveal(false);

        if !self.is_empty() {
            imp.self_stack.freeze();
        }

        GtkUtil::remove_source(imp.drag_released_motion_signal.write().take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.write().take());
        *imp.drag_ongoing.write() = false;

        *imp.load_start_timestamp.write() = Utc::now();

        let html = self.build_article(&article, &feed_name);
        let base_url = Self::get_base_url(&article);

        imp.visible_article.write().replace(article);
        imp.visible_feed_name.write().replace(feed_name);
        *imp.visible_article_enclosures.write() = enclosures;

        imp.view.read().load_html(&html, base_url.as_deref());
        imp.view.read().set_sensitive(true);

        if self.is_empty() {
            imp.stack.set_visible_child_name("article");
        } else {
            imp.self_stack.update(gtk4::StackTransitionType::Crossfade);
        }

        self.grab_focus();
    }

    pub fn redraw_article(&self) {
        let imp = imp::ArticleView::from_instance(self);

        if self.is_empty() || imp.visible_article.read().is_none() || imp.visible_feed_name.read().is_none() {
            warn!("Can't redraw article view. No article is on display.");
            return;
        }

        GtkUtil::remove_source(imp.drag_released_motion_signal.write().take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.write().take());
        *imp.drag_ongoing.write() = false;

        if let Some(article) = &*imp.visible_article.read() {
            if let Some(feed_name) = &*imp.visible_feed_name.read() {
                imp.self_stack.freeze();
                *imp.load_start_timestamp.write() = Utc::now();
                let html = self.build_article(&article, feed_name);
                imp.view
                    .read()
                    .load_html(&html, Self::get_base_url(&article).as_deref());
                imp.self_stack.update(gtk4::StackTransitionType::Crossfade);
                imp.view.read().set_sensitive(true);

                return;
            }
        }
    }

    fn is_empty(&self) -> bool {
        let imp = imp::ArticleView::from_instance(self);
        imp.stack
            .visible_child_name()
            .map(|s| s.as_str() != "article")
            .unwrap_or(false)
    }

    fn get_base_url(article: &FatArticle) -> Option<String> {
        if let Some(url) = &article.url {
            match url.origin() {
                Origin::Opaque(_op) => None,
                Origin::Tuple(scheme, host, port) => {
                    let host = match host {
                        Host::Domain(domain) => domain,
                        Host::Ipv4(ipv4) => ipv4.to_string(),
                        Host::Ipv6(ipv6) => ipv6.to_string(),
                    };
                    Some(format!("{}://{}:{}", scheme, host, port))
                }
            }
        } else {
            None
        }
    }

    pub fn get_visible_article(&self) -> (Option<FatArticle>, Option<Vec<Enclosure>>) {
        let imp = imp::ArticleView::from_instance(self);
        (
            (*imp.visible_article.read()).clone(),
            imp.visible_article_enclosures.read().clone(),
        )
    }

    pub fn update_visible_article(&self, read: Option<Read>, marked: Option<Marked>) {
        let imp = imp::ArticleView::from_instance(self);
        if let Some(visible_article) = &mut *imp.visible_article.write() {
            if let Some(marked) = marked {
                visible_article.marked = marked;
            }
            if let Some(read) = read {
                visible_article.unread = read;
            }
        }
    }

    pub fn close_article(&self) {
        let imp = imp::ArticleView::from_instance(self);
        imp.view.read().set_sensitive(false);
        imp.url_overlay_label.reveal(false);
        imp.progress_overlay_label.reveal(false);
        imp.visible_article.write().take();
        imp.visible_feed_name.write().take();
        imp.stack.set_visible_child_name("empty");
    }

    pub fn update_background_color(&self, color: &RGBA) {
        if (color.alpha() - 1.0).abs() < 0.01 {
            let imp = imp::ArticleView::from_instance(self);
            imp.view.read().set_background_color(color);
        }
    }

    fn on_crash(&self) {
        let imp = imp::ArticleView::from_instance(self);

        //hide overlays
        imp.progress_overlay_label.reveal(false);
        imp.url_overlay_label.reveal(false);

        GtkUtil::remove_source(imp.drag_released_motion_signal.write().take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.write().take());
        *imp.drag_ongoing.write() = false;

        // disconnect webview signals
        self.disconnect_view_signals();

        // replace webview
        let new_view = Self::new_webview(&imp.web_context.read());
        imp.self_stack.set_widget(&new_view);
        *imp.view.write() = new_view;
        self.connect_webview();

        imp.stack.set_visible_child_name("crash");
    }

    fn disconnect_view_signals(&self) {
        let imp = imp::ArticleView::from_instance(self);

        // disconnect signals
        GtkUtil::disconnect_signal(imp.decide_policy_signal.write().take(), &*imp.view.read());
        GtkUtil::disconnect_signal(imp.mouse_over_signal.write().take(), &*imp.view.read());
        GtkUtil::disconnect_signal(imp.ctx_menu_signal.write().take(), &*imp.view.read());
        GtkUtil::disconnect_signal(imp.load_signal.write().take(), &*imp.view.read());
    }

    fn new_webview(ctx: &WebContext) -> WebView {
        let settings = WebkitSettings::new();
        settings.set_enable_html5_database(false);
        settings.set_enable_html5_local_storage(false);
        settings.set_enable_java(false);
        settings.set_enable_page_cache(false);
        settings.set_enable_smooth_scrolling(false);
        settings.set_enable_javascript(true);
        settings.set_javascript_can_access_clipboard(false);
        settings.set_javascript_can_open_windows_automatically(false);
        settings.set_media_playback_requires_user_gesture(true);
        settings.set_user_agent_with_application_details(Some("NewsFlash"), None);
        settings.set_enable_developer_extras(App::default().settings().read().get_inspect_article_view());

        let webview = WebView::with_context(ctx);
        webview.set_settings(&settings);
        webview
    }

    fn connect_event_controller(&self) {
        let imp = imp::ArticleView::from_instance(self);

        imp.motion_event
            .connect_motion(clone!(@weak self as this => @default-panic, move |_controller, x, y| {
                let imp = imp::ArticleView::from_instance(&this);
                *imp.pointer_pos.write() = (x, y);
            }));

        //----------------------------------
        // zoom with ctrl+scroll
        //----------------------------------
        imp.scroll_event.connect_scroll(
            clone!(@weak self as this => @default-panic, move |event_controller, _x_delta, y_delta| {
                let imp = imp::ArticleView::from_instance(&this);

                if let Some(event) = event_controller.current_event() {
                    if event_controller.current_event_state().contains(ModifierType::CONTROL_MASK) {
                        let zoom = imp.view.read().zoom_level();
                        if let Ok(scroll_event) = event.downcast::<ScrollEvent>() {
                            match scroll_event.direction() {
                                ScrollDirection::Up => imp.view.read().set_zoom_level(zoom + 0.25),
                                ScrollDirection::Down => imp.view.read().set_zoom_level(zoom - 0.25),
                                ScrollDirection::Smooth => {
                                    let diff = 10.0 * (y_delta);
                                    imp.view.read().set_zoom_level(zoom - diff);
                                }
                                _ => {}
                            }
                            return Inhibit(true);
                        }

                        return Inhibit(false);
                    }
                }
                Inhibit(false)
            }),
        );

        //------------------------------------------------
        // zoom with ctrl+PLUS/MINUS & reset with ctrl+0
        //------------------------------------------------
        imp.key_event.connect_key_pressed(
            clone!(@weak self as this => @default-panic, move |_controller, key, _keyval, state| {
                let imp = imp::ArticleView::from_instance(&this);

                if state.contains(ModifierType::CONTROL_MASK) {
                    let zoom = imp.view.read().zoom_level();
                    match key {
                        Key::KP_0 => imp.view.read().set_zoom_level(1.0),
                        Key::KP_Add => imp.view.read().set_zoom_level(zoom + 0.25),
                        Key::KP_Subtract => imp.view.read().set_zoom_level(zoom - 0.25),
                        _ => return Inhibit(false),
                    }
                    return Inhibit(true);
                }
                Inhibit(false)
            }),
        );

        //------------------------------------------------
        // drag view with middle mouse button
        //------------------------------------------------
        imp.drag_gesture.connect_drag_begin(clone!(@weak self as this => @default-panic, move |gesture, _x, _y| {
            let imp = imp::ArticleView::from_instance(&this);

            gesture.set_state(EventSequenceState::Claimed);

            GtkUtil::remove_source(imp.drag_released_motion_signal.write().take());
            GtkUtil::remove_source(imp.drag_buffer_update_signal.write().take());
            Self::stop_scroll_animation(&imp.view.read(), &imp.scroll_animation_data);
            *imp.drag_buffer.write() = [0.0; 10];
            *imp.drag_ongoing.write() = true;
            *imp.drag_momentum.write() = 0.0;

            *imp.current_scroll_pos.write() = this.get_scroll_abs();

            imp.drag_buffer_update_signal.write().replace(
                glib::timeout_add_local(std::time::Duration::from_millis(10), clone!(@weak this => @default-panic, move ||
                {
                    let imp = imp::ArticleView::from_instance(&this);

                    if !*imp.drag_ongoing.read() {
                        imp.drag_buffer_update_signal.write().take();
                        return Continue(false);
                    }

                    for i in (1..10).rev() {
                        let value = (*imp.drag_buffer.read())[i - 1];
                        (*imp.drag_buffer.write())[i] = value;
                    }

                    (*imp.drag_buffer.write())[0] = *imp.drag_y_offset.read();
                    *imp.drag_momentum.write() = (*imp.drag_buffer.read())[9] - (*imp.drag_buffer.read())[0];
                    Continue(true)
                }))
            );

        }));

        imp.drag_gesture.connect_drag_update(
            clone!(@weak self as this => @default-panic, move |gesture, _x, y_offset| {
                let imp = imp::ArticleView::from_instance(&this);

                gesture.set_state(EventSequenceState::Claimed);

                for i in (1..10).rev() {
                    let value = (*imp.drag_buffer.read())[i - 1];
                    (*imp.drag_buffer.write())[i] = value;
                }

                (*imp.drag_buffer.write())[0] = y_offset;
                *imp.drag_momentum.write() = (*imp.drag_buffer.read())[9] - (*imp.drag_buffer.read())[0];

                let scroll = *imp.drag_y_offset.read() - y_offset;
                let scroll = scroll / imp.view.read().zoom_level();
                *imp.drag_y_offset.write() = y_offset;
                let scroll_pos = *imp.current_scroll_pos.read();
                let new_scroll_pos = scroll_pos + scroll;
                *imp.current_scroll_pos.write() = new_scroll_pos;
                this.set_scroll_abs(new_scroll_pos);
            }),
        );

        imp.drag_gesture.connect_drag_end(clone!(@weak self as this => @default-panic, move |gesture, _x, _y| {
            let imp = imp::ArticleView::from_instance(&this);
            *imp.drag_ongoing.write() = false;
            *imp.drag_y_offset.write() = 0.0;

            gesture.set_state(EventSequenceState::Claimed);

            let scroll_pos = Arc::new(RwLock::new(this.get_scroll_abs()));
            let scroll_upper = this.get_scroll_upper();

            imp.drag_released_motion_signal.write().replace(
                glib::timeout_add_local(std::time::Duration::from_millis(20), clone!(@weak this, @strong scroll_pos => @default-panic, move ||
                {
                    let imp = imp::ArticleView::from_instance(&this);

                    *imp.drag_momentum.write() /= 1.2;
                    let allocation = imp.view.read().allocation();

                    let page_size = f64::from(imp.view.read().allocated_height());
                    let adjust_value = page_size * *imp.drag_momentum.read() / f64::from(allocation.height());
                    let adjust_value = adjust_value / imp.view.read().zoom_level();
                    let old_adjust = *scroll_pos.read();
                    let upper = scroll_upper * imp.view.read().zoom_level();

                    if (old_adjust + adjust_value) > (upper - page_size)
                        || (old_adjust + adjust_value) < 0.0
                    {
                        *imp.drag_momentum.write() = 0.0;
                    }

                    let new_scroll_pos = f64::min(old_adjust + adjust_value, upper - page_size);
                    *scroll_pos.write() = new_scroll_pos;
                    this.set_scroll_abs(new_scroll_pos);

                    if imp.drag_momentum.read().abs() < 1.0 || *imp.drag_ongoing.read() {
                        imp.drag_released_motion_signal.write().take();
                        return Continue(false);
                    }

                    Continue(true)
                }))
            );
        }));
    }

    fn connect_webview(&self) {
        let imp = imp::ArticleView::from_instance(self);

        //----------------------------------
        // open link in external browser
        //----------------------------------
        imp.decide_policy_signal
            .write()
            .replace(
                imp.view
                    .read()
                    .connect_decide_policy(move |_closure_webivew, decision, decision_type| {
                        if decision_type == PolicyDecisionType::NewWindowAction {
                            if let Some(navigation_decision) = decision.downcast_ref::<NavigationPolicyDecision>() {
                                if let Some(frame_name) = navigation_decision.frame_name() {
                                    if &frame_name == "_blank" {
                                        if let Some(action) = navigation_decision.navigation_action() {
                                            if let Some(uri_req) = action.request() {
                                                if let Some(uri) = uri_req.uri() {
                                                    Util::send(Action::OpenUrlInDefaultBrowser(uri.as_str().into()));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            decision.ignore();
                            return true;
                        } else if decision_type == PolicyDecisionType::NavigationAction {
                            if let Some(navigation_decision) = decision.downcast_ref::<NavigationPolicyDecision>() {
                                if let Some(action) = navigation_decision.navigation_action() {
                                    if let Some(request) = action.request() {
                                        if let Some(uri) = request.uri() {
                                            if action.is_user_gesture() {
                                                decision.ignore();
                                                Util::send(Action::OpenUrlInDefaultBrowser(uri.as_str().into()));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        false
                    }),
            );

        //----------------------------------
        // show url overlay
        //----------------------------------
        imp.mouse_over_signal
            .write()
            .replace(imp.view.read().connect_mouse_target_changed(clone!(
                @weak self as this => @default-panic, move |_closure_webivew, hit_test, _modifiers|
            {
                let imp = imp::ArticleView::from_instance(&this);

                if hit_test.context_is_link() {
                    if let Some(uri) = hit_test.link_uri() {
                        let allocation = imp.stack.allocation();
                        let rel_x = imp.pointer_pos.read().0 / f64::from(allocation.width());
                        let rel_y = imp.pointer_pos.read().1 / f64::from(allocation.height());

                        let align = if rel_x <= 0.5 && rel_y >= 0.85 {
                            gtk4::Align::End
                        } else {
                            gtk4::Align::Start
                        };

                        imp.url_overlay_label.set_url(uri.as_str(), align);
                        imp.url_overlay_label.reveal(true);
                    }
                } else {
                    imp.url_overlay_label.reveal(false);
                }
            })));

        //----------------------------------
        // clean up context menu
        //----------------------------------
        imp.ctx_menu_signal
            .write()
            .replace(
                imp.view
                    .read()
                    .connect_context_menu(|_closure_webivew, ctx_menu, _event, _hit_test| {
                        let menu_items = ctx_menu.items();

                        for item in menu_items {
                            if item.is_separator() {
                                ctx_menu.remove(&item);
                                continue;
                            }

                            let remove_stock_actions = vec![
                                ContextMenuAction::CopyLinkToClipboard,
                                ContextMenuAction::Copy,
                                ContextMenuAction::CopyImageToClipboard,
                                ContextMenuAction::CopyImageUrlToClipboard,
                                ContextMenuAction::DownloadImageToDisk,
                                ContextMenuAction::InspectElement,
                            ];

                            if !remove_stock_actions.contains(&item.stock_action()) {
                                ctx_menu.remove(&item);
                            }
                        }

                        if ctx_menu.first().is_none() {
                            return true;
                        }

                        false
                    }),
            );

        //----------------------------------
        // display load progress
        //----------------------------------
        imp.load_signal
            .write()
            .replace(imp.view.read().connect_estimated_load_progress_notify(
                clone!(@weak self as this => @default-panic, move |closure_webivew| {
                    let imp = imp::ArticleView::from_instance(&this);

                    if Utc::now() < *imp.load_start_timestamp.read() + Duration::milliseconds(1500) {
                        imp.progress_overlay_label.reveal(false);
                        return;
                    }

                    let progress = closure_webivew.estimated_load_progress();
                    if (progress - 1.0).abs() < 0.01 {
                        imp.progress_overlay_label.reveal(false);
                        return;
                    }
                    imp.progress_overlay_label.reveal(true);
                    imp.progress_overlay_label.set_percentage(progress);
                }),
            ));

        //----------------------------------
        // crash view
        //----------------------------------
        imp.view.read().connect_web_process_terminated(
            clone!(@weak self as this => @default-panic, move |_closure_webivew, _reason|
            {
                this.on_crash();
            }),
        );

        //----------------------------------
        // fullscreen
        //----------------------------------
        imp.view.read().connect_enter_fullscreen(|_| {
            App::default().main_window().content_page().enter_video_fullscreen();
            false
        });
        imp.view.read().connect_leave_fullscreen(|_| {
            App::default().main_window().content_page().leave_video_fullscreen();
            false
        });
    }

    fn build_article(&self, article: &FatArticle, feed_name: &str) -> String {
        let prefer_dark_theme = libadwaita::StyleManager::default().is_dark();

        Self::build_article_static(
            "article",
            article,
            feed_name,
            Some(&App::default().settings()),
            None,
            None,
            None,
            App::default().content_page_state().read().get_prefer_scraped_content(),
            prefer_dark_theme,
        )
    }

    pub fn build_article_static(
        file_name: &str,
        article: &FatArticle,
        feed_name: &str,
        settings: Option<&Arc<RwLock<Settings>>>,
        theme_override: Option<ArticleTheme>,
        font_size_override: Option<i32>,
        selectable_override: Option<bool>,
        prefer_scraped_content: bool,
        prefer_dark_theme: bool,
    ) -> String {
        let template_data = ArticleViewResources::get(&format!("{}.html", file_name)).expect(GTK_RESOURCE_FILE_ERROR);
        let template_str = str::from_utf8(template_data.data.as_ref()).expect(GTK_RESOURCE_FILE_ERROR);
        let mut template_string = template_str.to_owned();

        let content_width = settings
            .map(|s| s.read().get_article_view_width())
            .flatten()
            .unwrap_or(constants::DEFAULT_ARTICLE_CONTENT_WIDTH);

        let line_height = settings
            .map(|s| s.read().get_article_view_line_height())
            .flatten()
            .unwrap_or(constants::DEFAULT_ARTICLE_LINE_HEIGHT);

        let css_data = ArticleViewResources::get("style.css").expect(GTK_RESOURCE_FILE_ERROR);
        let css_string: String = str::from_utf8(css_data.data.as_ref())
            .expect("Failed to load CSS from resources")
            .into();
        let css_string = css_string.replacen("$CONTENT_WIDTH", &format!("{}em", content_width), 1);
        let css_string = css_string.replacen("$LINE_HEIGHT", &format!("{}em", line_height), 1);

        // A list of fonts we should try to use in order of preference
        // We will pass all of these to CSS in order
        let mut font_options: Vec<String> = Vec::new();
        let mut font_families: Vec<String> = Vec::new();
        let mut font_size: Option<i32> = None;

        // Try to use the configured font if it exists
        if let Some(settings) = settings {
            if let Some(font_setting) = settings.read().get_article_view_font() {
                font_options.push(font_setting);
            }
        }

        // If there is no configured font, or it's broken, use the system default font
        let font_system = App::default().desktop_settings().document_font();
        font_options.push(font_system);

        // Backup if the system font is broken too
        font_options.push("sans".to_owned());

        for font in font_options {
            let desc = FontDescription::from_string(&font);
            if let Some(family) = desc.family() {
                font_families.push(family.to_string());
            }
            if font_size.is_none() && desc.size() > 0 {
                font_size = Some(desc.size());
            }
        }

        // if font size configured use it, otherwise use 12 as default
        let font_size = match font_size_override {
            Some(fsize_override) => fsize_override,
            None => match font_size {
                Some(size) => size,
                None => 12,
            },
        };

        let font_size = font_size / pango::SCALE;
        let font_family = font_families.join(", ");

        let mut author_date = String::new();
        let date = DateUtil::format(&article.date);
        if let Some(author) = &article.author {
            author_date.push_str(&format!("posted by: {}, {}", author, date));
        } else {
            author_date.push_str(&date);
        }

        // $HTML
        if prefer_scraped_content {
            if let Some(html) = &article.scraped_content {
                template_string = template_string.replacen("$HTML", html, 1);
            } else if let Some(html) = &article.html {
                template_string = template_string.replacen("$HTML", html, 1);
            }
        } else if let Some(html) = &article.html {
            template_string = template_string.replacen("$HTML", html, 1);
        }

        // $UNSELECTABLE
        if selectable_override.unwrap_or(
            settings
                .map(|s| s.read().get_article_view_allow_select())
                .unwrap_or(false),
        ) {
            template_string = template_string.replacen("$UNSELECTABLE", "", 2);
        } else {
            template_string = template_string.replacen("$UNSELECTABLE", "unselectable", 2);
        }

        // $AUTHOR / $DATE
        template_string = template_string.replacen("$AUTHOR", &author_date, 1);

        // $SMALLSIZE x2
        let small_size = font_size - 2;
        template_string = template_string.replacen("$SMALLSIZE", &format!("{}", small_size), 2);

        // $TITLE
        if let Some(title) = &article.title {
            template_string = template_string.replacen("$TITLE", title, 1);
        }

        // $LARGESIZE
        let large_size = font_size * 2;
        template_string = template_string.replacen("$LARGESIZE", &format!("{}", large_size), 1);

        // $URL
        if let Some(article_url) = &article.url {
            template_string = template_string.replacen("$URL", article_url.as_str(), 1);
        }

        // $FEED
        template_string = template_string.replacen("$FEED", feed_name, 1);

        // $THEME
        let theme = if let Some(theme_override) = &theme_override {
            theme_override.to_str(prefer_dark_theme).to_owned()
        } else {
            if let Some(settings) = settings {
                settings
                    .read()
                    .get_article_view_theme()
                    .to_str(prefer_dark_theme)
                    .to_owned()
            } else {
                ArticleTheme::Default.to_str(prefer_dark_theme).to_owned()
            }
        };
        template_string = template_string.replacen("$THEME", &theme, 1);

        // $FONTFAMILY
        template_string = template_string.replacen("$FONTFAMILY", &font_family, 1);

        // $FONTSIZE
        template_string = template_string.replacen("$FONTSIZE", &format!("{}", font_size), 1);

        // $CSS
        template_string = template_string.replacen("$CSS", &css_string, 1);

        template_string
    }

    fn set_scroll_pos_static(view: &WebView, pos: f64) {
        let cancellable: Option<&Cancellable> = None;
        view.run_javascript(&format!("window.scrollTo(0,{});", pos), cancellable, |res| match res {
            Ok(_) => {}
            Err(_) => error!("Setting scroll pos failed"),
        });
    }

    fn get_scroll_pos_static(view: &WebView) -> f64 {
        Self::webview_js_get_f64(view, "window.scrollY").expect("Failed to get scroll position from webview.")
    }

    fn get_scroll_window_height_static(view: &WebView) -> f64 {
        Self::webview_js_get_f64(view, "window.innerHeight").expect("Failed to get window height from webview.")
    }

    fn get_scroll_upper_static(view: &WebView) -> f64 {
        Self::webview_js_get_f64(
            view,
            "Math.max (
            document.body.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.clientHeight,
            document.documentElement.scrollHeight,
            document.documentElement.offsetHeight
        )",
        )
        .expect("Failed to get upper limit from webview.")
    }

    fn webview_js_get_f64(view: &WebView, java_script: &str) -> Result<f64, ArticleViewError> {
        let wait_loop = Arc::new(MainLoop::new(None, false));
        let value: Arc<RwLock<Option<f64>>> = Arc::new(RwLock::new(None));
        let cancellable: Option<&Cancellable> = None;
        view.run_javascript(
            java_script,
            cancellable,
            clone!(@weak wait_loop, @weak value => @default-panic, move |res| {
                match res {
                    Ok(result) => {
                        let new_value = result.js_value().expect("Failed to get value from js result.");
                        *value.write() = Some(new_value.to_double());
                    }
                    Err(_) => error!("Getting scroll pos failed"),
                }
                wait_loop.quit();
            }),
        );

        wait_loop.run();

        let output = if let Some(pos) = *value.read() {
            Ok(pos)
        } else {
            Err(ArticleViewErrorKind::NoValueFromJS.into())
        };

        output
    }

    fn set_scroll_abs(&self, scroll: f64) {
        let imp = imp::ArticleView::from_instance(self);
        Self::set_scroll_pos_static(&imp.view.read(), scroll);
    }

    fn get_scroll_abs(&self) -> f64 {
        let imp = imp::ArticleView::from_instance(self);
        Self::get_scroll_pos_static(&imp.view.read())
    }

    fn get_scroll_window_height(&self) -> f64 {
        let imp = imp::ArticleView::from_instance(self);
        Self::get_scroll_window_height_static(&imp.view.read())
    }

    fn get_scroll_upper(&self) -> f64 {
        let imp = imp::ArticleView::from_instance(self);
        Self::get_scroll_upper_static(&imp.view.read())
    }

    pub fn animate_scroll_diff(&self, diff: f64) -> Result<(), ArticleViewError> {
        let pos = self.get_scroll_abs();
        let upper = self.get_scroll_upper();
        let window_height = self.get_scroll_window_height();

        if (pos <= 0.0 && diff.is_sign_negative()) || (pos >= (upper - window_height) && diff.is_sign_positive()) {
            return Ok(());
        }

        self.animate_scroll_absolute(pos + diff, pos)
    }

    fn animate_scroll_absolute(&self, target: f64, current_pos: f64) -> Result<(), ArticleViewError> {
        let animate = match gtk4::Settings::default() {
            Some(settings) => settings.is_gtk_enable_animations(),
            None => false,
        };

        if !self.is_mapped() || !animate {
            self.set_scroll_abs(target);
            return Ok(());
        }

        let imp = imp::ArticleView::from_instance(self);

        *imp.scroll_animation_data.start_time.write() = self.frame_clock().map(|clock| clock.frame_time());
        *imp.scroll_animation_data.end_time.write() = self
            .frame_clock()
            .map(|clock| clock.frame_time() + SCROLL_TRANSITION_DURATION);

        let callback_id = imp.scroll_animation_data.scroll_callback_id.write().take();
        let leftover_scroll = match callback_id {
            Some(callback_id) => {
                callback_id.remove();
                let start_value = Util::some_or_default(*imp.scroll_animation_data.transition_start_value.read(), 0.0);
                let diff_value = Util::some_or_default(*imp.scroll_animation_data.transition_diff.read(), 0.0);
                start_value + diff_value - current_pos
            }
            None => 0.0,
        };

        let diff = if (target + 1.0).abs() < 0.001 {
            -current_pos
        } else {
            (target - current_pos) + leftover_scroll
        };

        imp.scroll_animation_data.transition_diff.write().replace(diff);

        imp.scroll_animation_data
            .transition_start_value
            .write()
            .replace(current_pos);

        imp.scroll_animation_data
            .scroll_callback_id
            .write()
            .replace(imp.view.read().add_tick_callback(clone!(
                @strong imp.scroll_animation_data as scroll_animation_data => @default-panic, move |view, clock|
            {
                let start_value = Util::some_or_default(*scroll_animation_data.transition_start_value.read(), 0.0);
                let diff_value = Util::some_or_default(*scroll_animation_data.transition_diff.read(), 0.0);
                let now = clock.frame_time();
                let end_time_value = Util::some_or_default(*scroll_animation_data.end_time.read(), 0);
                let start_time_value = Util::some_or_default(*scroll_animation_data.start_time.read(), 0);

                if !view.is_mapped() {
                    Self::set_scroll_pos_static(&view, start_value + diff_value);
                    return Continue(false);
                }

                if scroll_animation_data.end_time.read().is_none() {
                    return Continue(false);
                }

                let t = if now < end_time_value {
                    (now - start_time_value) as f64 / (end_time_value - start_time_value) as f64
                } else {
                    1.0
                };

                let t = Util::ease_out_cubic(t);

                Self::set_scroll_pos_static(&view, start_value + (t * diff_value));

                let pos = Self::get_scroll_pos_static(&view);
                let upper = Self::get_scroll_upper_static(&view);
                let window_height = Self::get_scroll_window_height_static(&view);

                if (pos <= 0.0 && diff_value.is_sign_negative())
                || (pos + window_height >= upper && diff_value.is_sign_positive())
                || now >= end_time_value {
                    Self::stop_scroll_animation(&view, &scroll_animation_data);
                    return Continue(false);
                }

                Continue(true)
            })));

        Ok(())
    }

    fn stop_scroll_animation(view: &WebView, properties: &ScrollAnimationProperties) {
        if let Some(callback_id) = properties.scroll_callback_id.write().take() {
            callback_id.remove();
        }
        view.queue_draw();
        properties.transition_start_value.write().take();
        properties.transition_diff.write().take();
        properties.start_time.write().take();
        properties.end_time.write().take();
    }

    pub fn clear_cache(&self, oneshot_sender: OneShotSender<()>) {
        let imp = imp::ArticleView::from_instance(self);
        if let Some(data_manager) = imp.web_context.read().website_data_manager() {
            let cancellable: Option<&Cancellable> = None;
            data_manager.clear(
                WebsiteDataTypes::all(),
                glib::TimeSpan::from_seconds(0),
                cancellable,
                move |res| {
                    if let Err(error) = res {
                        log::error!("Failed to clear webkit cache: {}", error);
                    }
                    oneshot_sender.send(()).expect(CHANNEL_ERROR);
                },
            );
        }
    }
}
