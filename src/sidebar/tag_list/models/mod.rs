mod error;
mod tag;
mod tag_gobject;

use self::error::{TagListModelError, TagListModelErrorKind};
use crate::sidebar::SidebarIterateItem;
use diffus::{edit::Edit, Diffable};
use news_flash::models::{Tag, TagID};
use std::collections::HashSet;
use std::sync::Arc;
pub use tag::TagListTagModel;
pub use tag_gobject::TagGObject;

#[derive(Clone, Debug)]
pub struct TagListModel {
    models: Vec<TagListTagModel>,
    tags: HashSet<Arc<TagID>>,
}

impl TagListModel {
    pub fn new() -> Self {
        TagListModel {
            models: Vec::new(),
            tags: HashSet::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.tags.is_empty()
    }

    pub fn add(&mut self, tag: &Tag) -> Result<(), TagListModelError> {
        if self.tags.contains(&tag.tag_id) {
            return Err(TagListModelErrorKind::AlreadyExists.into());
        }
        let model = TagListTagModel::new(tag);
        self.tags.insert(model.id.clone());
        self.models.push(model);
        Ok(())
    }

    pub fn generate_diff<'a>(&'a self, other: &'a TagListModel) -> Edit<'_, Vec<TagListTagModel>> {
        self.models.diff(&other.models)
    }

    pub fn sort(&mut self) {
        self.models.sort_by(|a, b| a.cmp(b));
    }

    pub fn calculate_next_item(&self, selected_id: &Arc<TagID>) -> SidebarIterateItem {
        match self.models.iter().position(|item| &item.id == selected_id) {
            None => SidebarIterateItem::SelectAll,
            Some(pos) => match self.models.iter().skip(pos + 1).next() {
                None => SidebarIterateItem::SelectAll,
                Some(item) => SidebarIterateItem::SelectTagList(item.id.clone()),
            },
        }
    }

    pub fn calculate_prev_item(&self, selected_id: &Arc<TagID>) -> SidebarIterateItem {
        match self.models.iter().position(|item| &item.id == selected_id) {
            None => SidebarIterateItem::FeedListSelectLastItem,
            Some(pos) => {
                if pos == 0 {
                    SidebarIterateItem::FeedListSelectLastItem
                } else {
                    match self.models.iter().skip(pos - 1).next() {
                        None => SidebarIterateItem::SelectAll,
                        Some(item) => SidebarIterateItem::SelectTagList(item.id.clone()),
                    }
                }
            }
        }
    }

    pub fn first(&self) -> Option<TagListTagModel> {
        self.models.first().cloned()
    }

    pub fn last(&self) -> Option<TagListTagModel> {
        self.models.last().cloned()
    }
}
