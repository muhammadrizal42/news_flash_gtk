use super::popover_tag_gobject::PopoverTagGObject;
use crate::util::{constants, GtkUtil};
use glib::{clone, subclass::*, ParamFlags, ParamSpec, ParamSpecBoolean, ParamSpecString, Value};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, DrawingArea, GestureClick, Image, Label};
use news_flash::models::TagID;
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use std::sync::Arc;

static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
    vec![Signal::builder(
        "activated",
        &[PopoverTagRow::static_type().into()],
        <()>::static_type().into(),
    )
    .build()]
});

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/popover_tag.ui")]
    pub struct PopoverTagRow {
        pub id: RwLock<Arc<TagID>>,
        pub color: Arc<RwLock<String>>,

        #[template_child]
        pub tag_title: TemplateChild<Label>,
        #[template_child]
        pub tag_color: TemplateChild<DrawingArea>,
        #[template_child]
        pub image: TemplateChild<Image>,
        #[template_child]
        pub row_activate: TemplateChild<GestureClick>,
    }

    impl Default for PopoverTagRow {
        fn default() -> Self {
            PopoverTagRow {
                id: RwLock::new(Arc::new(TagID::new(""))),
                color: Arc::new(RwLock::new(constants::TAG_DEFAULT_INNER_COLOR.into())),

                tag_title: TemplateChild::default(),
                tag_color: TemplateChild::default(),
                image: TemplateChild::default(),
                row_activate: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PopoverTagRow {
        const NAME: &'static str = "PopoverTagRow";
        type ParentType = gtk4::Box;
        type Type = super::PopoverTagRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PopoverTagRow {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new("title", "title", "title", None, ParamFlags::READWRITE),
                    ParamSpecString::new("color", "color", "color", None, ParamFlags::READWRITE),
                    ParamSpecBoolean::new("assigned", "assigned", "assigned", false, ParamFlags::READWRITE),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "title" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.tag_title.set_label(&input);
                }
                "color" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    *self.color.write() = input;
                    self.tag_color.queue_draw();
                }
                "assigned" => {
                    let input: bool = value.get().expect("The value needs to be of type `bool`.");
                    self.image.set_visible(input);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "title" => obj.get_title().to_value(),
                "color" => self.color.read().clone().to_value(),
                "assigned" => self.image.get_visible().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for PopoverTagRow {}

    impl BoxImpl for PopoverTagRow {}
}

glib::wrapper! {
    pub struct PopoverTagRow(ObjectSubclass<imp::PopoverTagRow>)
        @extends gtk4::Widget, gtk4::Box;
}

impl PopoverTagRow {
    pub fn new() -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        row.init();
        row
    }

    fn init(&self) {
        let imp = imp::PopoverTagRow::from_instance(self);

        imp.row_activate.connect_pressed(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            this.activate();
        }));
    }

    pub fn bind_model(&self, model: &PopoverTagGObject) {
        let imp = imp::PopoverTagRow::from_instance(self);
        let is_same_tag = *imp.id.read() == model.tag_id();

        *imp.id.write() = model.tag_id();
        *imp.color.write() = model.color();

        imp.tag_color.set_draw_func(
            clone!(@strong imp.color as color => @default-panic, move |_drawing_area, ctx, _width, _height| {
                GtkUtil::draw_color_cirlce(&ctx, &*color.read());
            }),
        );

        if !is_same_tag {
            imp.tag_title.set_label(&model.title());
        }
    }

    fn get_title(&self) -> String {
        let imp = imp::PopoverTagRow::from_instance(self);
        imp.tag_title.text().as_str().to_string()
    }

    fn activate(&self) {
        self.emit_by_name::<()>("activated", &[&self.clone()]);
    }
}
