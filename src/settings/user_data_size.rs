use news_flash::models::DatabaseSize;

#[derive(Debug)]
pub struct UserDataSize {
    pub database: DatabaseSize,
    pub webkit: u64,
}
