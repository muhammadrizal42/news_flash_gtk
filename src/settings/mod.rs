mod advanced;
mod article_list;
mod article_view;
mod dialog;
mod error;
mod feed_list;
mod general;
mod keybindings;
mod user_data_size;

pub use self::advanced::{AdvancedSettings, ProxyModel, ProxyProtocoll};
use self::error::{SettingsError, SettingsErrorKind};
use self::general::SyncInterval;
pub use self::user_data_size::UserDataSize;
use crate::article_view::ArticleTheme;
use article_list::ArticleListSettings;
use article_view::ArticleViewSettings;
pub use dialog::SettingsDialog;
use failure::ResultExt;
use feed_list::FeedListSettings;
use general::GeneralSettings;
pub use keybindings::Keybindings;
use news_flash::models::ArticleOrder;
use serde::{Deserialize, Serialize};
use std::fs;
use std::path::PathBuf;

static CONFIG_NAME: &str = "newsflash_gtk.json";

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Settings {
    general: GeneralSettings,
    advanced: AdvancedSettings,
    #[serde(default)]
    feed_list: FeedListSettings,
    article_list: ArticleListSettings,
    article_view: ArticleViewSettings,
    keybindings: Keybindings,
    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    path: PathBuf,
}

impl Settings {
    pub fn open() -> Result<Self, SettingsError> {
        let path = crate::app::CONFIG_DIR.join(CONFIG_NAME);
        if path.as_path().exists() {
            let data = fs::read_to_string(&path).context(SettingsErrorKind::ReadFromDisk)?;
            if let Ok(mut settings) = serde_json::from_str::<Self>(&data).context(SettingsErrorKind::InvalidJsonContent)
            {
                settings.path = path;
                return Ok(settings);
            }
        }

        fs::create_dir_all(crate::app::CONFIG_DIR.as_path()).context(SettingsErrorKind::CreateDirectory)?;

        let settings = Settings {
            general: GeneralSettings::default(),
            advanced: AdvancedSettings::default(),
            feed_list: FeedListSettings::default(),
            article_list: ArticleListSettings::default(),
            article_view: ArticleViewSettings::default(),
            keybindings: Keybindings::default(),
            path,
        };
        settings.write().context(SettingsErrorKind::WriteToDisk)?;
        Ok(settings)
    }

    fn write(&self) -> Result<(), SettingsError> {
        let data = serde_json::to_string_pretty(self).context(SettingsErrorKind::Serialize)?;
        fs::write(&self.path, data).context(SettingsErrorKind::WriteToDisk)?;
        Ok(())
    }

    pub fn get_keep_running_in_background(&self) -> bool {
        self.general.keep_running_in_background
    }

    pub fn set_keep_running_in_background(&mut self, keep_running: bool) -> Result<(), SettingsError> {
        self.general.keep_running_in_background = keep_running;
        self.write()?;
        Ok(())
    }

    pub fn get_sync_on_startup(&self) -> bool {
        self.general.sync_on_startup
    }

    pub fn set_sync_on_startup(&mut self, sync_on_startup: bool) -> Result<(), SettingsError> {
        self.general.sync_on_startup = sync_on_startup;
        self.write()?;
        Ok(())
    }

    pub fn get_sync_interval(&self) -> SyncInterval {
        self.general.sync_every
    }

    pub fn set_sync_interval(&mut self, sync_every: SyncInterval) -> Result<(), SettingsError> {
        self.general.sync_every = sync_every;
        self.write()?;
        Ok(())
    }

    pub fn get_feed_list_only_show_relevant(&self) -> bool {
        self.feed_list.only_show_relevant
    }

    pub fn set_feed_list_only_show_relevant(&mut self, only_show_relevant: bool) -> Result<(), SettingsError> {
        self.feed_list.only_show_relevant = only_show_relevant;
        self.write()?;
        Ok(())
    }

    pub fn get_article_list_order(&self) -> ArticleOrder {
        self.article_list.order.clone()
    }

    pub fn set_article_list_order(&mut self, order: ArticleOrder) -> Result<(), SettingsError> {
        self.article_list.order = order;
        self.write()?;
        Ok(())
    }

    pub fn get_article_list_show_thumbs(&self) -> bool {
        self.article_list.show_thumbnails
    }

    pub fn set_article_list_show_thumbs(&mut self, show_thumbs: bool) -> Result<(), SettingsError> {
        self.article_list.show_thumbnails = show_thumbs;
        self.write()?;
        Ok(())
    }

    pub fn get_article_list_hide_future_articles(&self) -> bool {
        self.article_list.hide_future_articles
    }

    pub fn set_article_list_hide_future_articles(&mut self, hide_future_articles: bool) -> Result<(), SettingsError> {
        self.article_list.hide_future_articles = hide_future_articles;
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_theme(&self) -> ArticleTheme {
        self.article_view.theme.clone()
    }

    pub fn set_article_view_theme(&mut self, theme: &ArticleTheme) -> Result<(), SettingsError> {
        self.article_view.theme = theme.clone();
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_allow_select(&self) -> bool {
        self.article_view.allow_select
    }

    pub fn set_article_view_allow_select(&mut self, allow: bool) -> Result<(), SettingsError> {
        self.article_view.allow_select = allow;
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_font(&self) -> Option<String> {
        self.article_view.font.clone()
    }

    pub fn set_article_view_font(&mut self, font: Option<String>) -> Result<(), SettingsError> {
        self.article_view.font = font;
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_width(&self) -> Option<u32> {
        self.article_view.content_width.clone()
    }

    pub fn set_article_view_width(&mut self, width: Option<u32>) -> Result<(), SettingsError> {
        self.article_view.content_width = width;
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_line_height(&self) -> Option<f32> {
        self.article_view.line_height.clone()
    }

    pub fn set_article_view_line_height(&mut self, height: Option<f32>) -> Result<(), SettingsError> {
        self.article_view.line_height = height;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_shortcut(&self) -> Option<String> {
        self.keybindings.general.shortcut.clone()
    }

    pub fn set_keybind_shortcut(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.general.shortcut = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_refresh(&self) -> Option<String> {
        self.keybindings.general.refresh.clone()
    }

    pub fn set_keybind_refresh(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.general.refresh = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_search(&self) -> Option<String> {
        self.keybindings.general.search.clone()
    }

    pub fn set_keybind_search(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.general.search = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_quit(&self) -> Option<String> {
        self.keybindings.general.quit.clone()
    }

    pub fn set_keybind_quit(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.general.quit = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_all_articles(&self) -> Option<String> {
        self.keybindings.general.all_articles.clone()
    }

    pub fn set_keybind_all_articles(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.general.all_articles = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_only_unread(&self) -> Option<String> {
        self.keybindings.general.only_unread.clone()
    }

    pub fn set_keybind_only_unread(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.general.only_unread = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_only_starred(&self) -> Option<String> {
        self.keybindings.general.only_starred.clone()
    }

    pub fn set_keybind_only_starred(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.general.only_starred = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_up(&self) -> Option<String> {
        self.keybindings.article_view.scroll_up.clone()
    }

    pub fn set_keybind_article_view_up(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_view.scroll_up = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_down(&self) -> Option<String> {
        self.keybindings.article_view.scroll_down.clone()
    }

    pub fn set_keybind_article_view_down(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_view.scroll_down = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_scrap(&self) -> Option<String> {
        self.keybindings.article_view.scrap_content.clone()
    }

    pub fn set_keybind_article_view_scrap(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_view.scrap_content = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_tag(&self) -> Option<String> {
        self.keybindings.article_view.tag.clone()
    }

    pub fn set_keybind_article_view_tag(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_view.tag = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_next(&self) -> Option<String> {
        self.keybindings.article_list.next.clone()
    }

    pub fn set_keybind_article_list_next(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_list.next = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_prev(&self) -> Option<String> {
        self.keybindings.article_list.prev.clone()
    }

    pub fn set_keybind_article_list_prev(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_list.prev = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_read(&self) -> Option<String> {
        self.keybindings.article_list.read.clone()
    }

    pub fn set_keybind_article_list_read(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_list.read = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_mark(&self) -> Option<String> {
        self.keybindings.article_list.mark.clone()
    }

    pub fn set_keybind_article_list_mark(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_list.mark = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_open(&self) -> Option<String> {
        self.keybindings.article_list.open.clone()
    }

    pub fn set_keybind_article_list_open(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.article_list.open = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_feed_list_next(&self) -> Option<String> {
        self.keybindings.feed_list.next.clone()
    }

    pub fn set_keybind_feed_list_next(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.feed_list.next = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_feed_list_prev(&self) -> Option<String> {
        self.keybindings.feed_list.prev.clone()
    }

    pub fn set_keybind_feed_list_prev(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.feed_list.prev = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_feed_list_toggle_expanded(&self) -> Option<String> {
        self.keybindings.feed_list.toggle_expanded.clone()
    }

    pub fn set_keybind_feed_list_toggle_expanded(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.feed_list.toggle_expanded = key;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_sidebar_set_read(&self) -> Option<String> {
        self.keybindings.feed_list.read.clone()
    }

    pub fn set_keybind_sidebar_set_read(&mut self, key: Option<String>) -> Result<(), SettingsError> {
        self.keybindings.feed_list.read = key;
        self.write()?;
        Ok(())
    }

    pub fn get_accept_invalid_certs(&self) -> bool {
        self.advanced.accept_invalid_certs
    }

    pub fn set_accept_invalid_certs(&mut self, accept_invalid_certs: bool) -> Result<(), SettingsError> {
        self.advanced.accept_invalid_certs = accept_invalid_certs;
        self.write()?;
        Ok(())
    }

    pub fn get_inspect_article_view(&self) -> bool {
        self.advanced.inspect_article_view
    }

    pub fn set_inspect_article_view(&mut self, inspect_article_view: bool) {
        self.advanced.inspect_article_view = inspect_article_view;
    }

    pub fn get_accept_invalid_hostnames(&self) -> bool {
        self.advanced.accept_invalid_hostnames
    }

    pub fn set_accept_invalid_hostnames(&mut self, accept_invalid_hostnames: bool) -> Result<(), SettingsError> {
        self.advanced.accept_invalid_hostnames = accept_invalid_hostnames;
        self.write()?;
        Ok(())
    }

    pub fn get_proxy(&self) -> Vec<ProxyModel> {
        self.advanced.proxy.clone()
    }
}
