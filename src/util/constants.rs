pub const TAG_DEFAULT_OUTER_COLOR: &str = "#FF0077";
pub const TAG_DEFAULT_INNER_COLOR: &str = "#FF0077";
pub const UNKNOWN_FEED: &str = "Unknown Feed";
pub const DEFAULT_ARTICLE_CONTENT_WIDTH: u32 = 50;
pub const DEFAULT_ARTICLE_LINE_HEIGHT: f32 = 1.8;
