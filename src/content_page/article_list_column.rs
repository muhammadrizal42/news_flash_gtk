use super::article_list_mode::ArticleListMode;
use crate::app::{Action, App};
use crate::article_list::ArticleList;
use crate::content_page::{OfflinePopover, OnlinePopover};
use crate::util::Util;
use glib::{clone, Continue, SourceId};
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, SearchBar, SearchEntry, Stack, ToggleButton};
use libadwaita::{HeaderBar, ViewStack};
use parking_lot::RwLock;
use std::sync::Arc;
use std::time::Duration;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_list_column.ui")]
    pub struct ArticleListColumn {
        #[template_child]
        pub headerbar: TemplateChild<HeaderBar>,
        #[template_child]
        pub update_button: TemplateChild<Button>,
        #[template_child]
        pub offline_button: TemplateChild<Button>,
        #[template_child]
        pub update_stack: TemplateChild<Stack>,
        #[template_child]
        pub search_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub search_entry: TemplateChild<SearchEntry>,
        #[template_child]
        pub search_bar: TemplateChild<SearchBar>,
        #[template_child]
        pub mark_all_read_button: TemplateChild<Button>,
        #[template_child]
        pub mark_all_read_stack: TemplateChild<Stack>,
        #[template_child]
        pub back_button: TemplateChild<Button>,
        #[template_child]
        pub view_switcher_stack: TemplateChild<ViewStack>,
        #[template_child]
        pub article_list: TemplateChild<ArticleList>,

        pub online_popover: RwLock<Option<OnlinePopover>>,
        pub offline_popover: RwLock<Option<OfflinePopover>>,
    }

    impl Default for ArticleListColumn {
        fn default() -> Self {
            Self {
                headerbar: TemplateChild::default(),
                update_button: TemplateChild::default(),
                offline_button: TemplateChild::default(),
                update_stack: TemplateChild::default(),
                search_button: TemplateChild::default(),
                search_entry: TemplateChild::default(),
                search_bar: TemplateChild::default(),
                mark_all_read_button: TemplateChild::default(),
                mark_all_read_stack: TemplateChild::default(),
                back_button: TemplateChild::default(),
                view_switcher_stack: TemplateChild::default(),
                article_list: TemplateChild::default(),

                online_popover: RwLock::new(None),
                offline_popover: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleListColumn {
        const NAME: &'static str = "ArticleListColumn";
        type ParentType = gtk4::Box;
        type Type = super::ArticleListColumn;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleListColumn {}

    impl WidgetImpl for ArticleListColumn {}

    impl BoxImpl for ArticleListColumn {}
}

glib::wrapper! {
    pub struct ArticleListColumn(ObjectSubclass<imp::ArticleListColumn>)
        @extends gtk4::Widget, gtk4::Box;
}

impl ArticleListColumn {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let imp = imp::ArticleListColumn::from_instance(self);

        let online_popover = OnlinePopover::new();
        online_popover.set_parent(&*imp.update_stack);
        imp.online_popover.write().replace(online_popover);

        let offline_popover = OfflinePopover::new();
        offline_popover.set_parent(&*imp.offline_button);
        imp.offline_popover.write().replace(offline_popover);

        imp.update_button.connect_clicked(move |_button| {
            Util::send(Action::Sync);
        });
        imp.offline_button.connect_clicked(move |_button| {
            Util::send(Action::SetOfflineMode(false));
        });
        let mark_all_read_stack = imp.mark_all_read_stack.get();
        imp.mark_all_read_button.connect_clicked(move |button| {
            button.set_sensitive(false);
            mark_all_read_stack.set_visible_child_name("spinner");
            Util::send(Action::SetSidebarRead);
        });

        imp.article_list.init(&imp.search_bar);

        self.setup_view_switcher();
        Self::setup_search_button(&imp.search_button, &imp.search_bar);
        Self::setup_search_bar(&imp.search_bar, &imp.search_button, &imp.search_entry);
        Self::setup_search_entry(&imp.search_entry);
    }

    pub fn article_list(&self) -> &ArticleList {
        let imp = imp::ArticleListColumn::from_instance(self);
        &imp.article_list
    }

    pub fn headerbar(&self) -> &HeaderBar {
        let imp = imp::ArticleListColumn::from_instance(self);
        &imp.headerbar
    }

    pub fn back_button(&self) -> &Button {
        let imp = imp::ArticleListColumn::from_instance(self);
        &imp.back_button
    }

    pub fn update_stack(&self) -> &Stack {
        let imp = imp::ArticleListColumn::from_instance(self);
        &imp.update_stack
    }

    pub fn offline_button(&self) -> &Button {
        let imp = imp::ArticleListColumn::from_instance(self);
        &imp.offline_button
    }

    pub fn set_view_switcher_stack(&self, child_name: &str) {
        let imp = imp::ArticleListColumn::from_instance(self);
        imp.view_switcher_stack
            .set_visible_child_name(&format!("{}_placeholder", child_name));
    }

    fn setup_view_switcher(&self) {
        let imp = imp::ArticleListColumn::from_instance(self);

        let view_switcher_timeout: Arc<RwLock<Option<SourceId>>> = Arc::new(RwLock::new(None));
        let article_list_mode = Arc::new(RwLock::new(ArticleListMode::All));

        imp.view_switcher_stack.connect_visible_child_name_notify(clone!(
            @strong article_list_mode,
            @strong view_switcher_timeout,
            @strong self as column => @default-panic, move |stack|
        {
            if let Some(child_name) = stack.visible_child_name() {
                let child_name = child_name.to_string();

                *article_list_mode.write() = if child_name.starts_with("unread") {
                    ArticleListMode::Unread
                } else if child_name.starts_with("marked") {
                    ArticleListMode::Marked
                } else {
                    ArticleListMode::All
                };

                if view_switcher_timeout.read().is_some() {
                    return;
                }

                column.stack_switched(&article_list_mode, &view_switcher_timeout);
            }
        }));
    }

    fn stack_switched(
        &self,
        article_list_mode: &Arc<RwLock<ArticleListMode>>,
        view_switcher_timeout: &Arc<RwLock<Option<SourceId>>>,
    ) {
        self.set_mode((*article_list_mode.read()).clone());

        if view_switcher_timeout.read().is_some() {
            return;
        }

        let mode_before_cooldown = (*article_list_mode.read()).clone();
        view_switcher_timeout.write().replace(glib::timeout_add_local(
            Duration::from_millis(250),
            clone!(
                @strong article_list_mode,
                @strong view_switcher_timeout,
                @strong self as column => @default-panic, move ||
            {
                view_switcher_timeout.write().take();
                if mode_before_cooldown != *article_list_mode.read() {
                    column.stack_switched(
                        &article_list_mode,
                        &view_switcher_timeout,
                    );
                }
                Continue(false)
            }),
        ));
    }

    fn set_mode(&self, new_mode: ArticleListMode) {
        let content_page_state = App::default().content_page_state();
        let old_selection = content_page_state.read().get_article_list_mode().clone();
        content_page_state.write().set_article_list_mode(new_mode.clone());
        match new_mode {
            ArticleListMode::All => self.set_view_switcher_stack("all"),
            ArticleListMode::Unread => self.set_view_switcher_stack("unread"),
            ArticleListMode::Marked => self.set_view_switcher_stack("marked"),
        };
        Util::send(Action::UpdateArticleList);

        let update_sidebar = match old_selection {
            ArticleListMode::All | ArticleListMode::Unread => match new_mode {
                ArticleListMode::All | ArticleListMode::Unread => false,
                ArticleListMode::Marked => true,
            },
            ArticleListMode::Marked => match new_mode {
                ArticleListMode::All | ArticleListMode::Unread => true,
                ArticleListMode::Marked => false,
            },
        };
        if update_sidebar {
            Util::send(Action::UpdateSidebar);
        }
    }

    fn setup_search_button(search_button: &ToggleButton, search_bar: &SearchBar) {
        search_button.connect_toggled(clone!(@weak search_bar => @default-panic, move |button| {
            if button.is_active() {
                search_bar.set_search_mode(true);
            } else {
                search_bar.set_search_mode(false);
            }
        }));
    }

    fn setup_search_bar(search_bar: &SearchBar, search_button: &ToggleButton, search_entry: &SearchEntry) {
        search_bar.connect_entry(search_entry);
        search_bar.connect_search_mode_enabled_notify(
            clone!(@weak search_button => @default-panic, move |search_bar| {
                if !search_bar.is_search_mode() {
                    search_button.set_active(false);
                }
            }),
        );
    }

    fn setup_search_entry(search_entry: &SearchEntry) {
        search_entry.connect_search_changed(move |search_entry| {
            Util::send(Action::SearchTerm(search_entry.text().as_str().into()));
        });
    }

    pub fn start_sync(&self) {
        let imp = imp::ArticleListColumn::from_instance(self);
        imp.update_stack.set_visible_child_name("spinner");
    }

    pub fn finish_sync(&self) {
        let imp = imp::ArticleListColumn::from_instance(self);
        imp.update_stack.set_visible_child_name("button");
    }

    pub fn finish_mark_all_read(&self) {
        let imp = imp::ArticleListColumn::from_instance(self);
        imp.mark_all_read_button.set_sensitive(true);
        imp.mark_all_read_stack.set_visible_child_name("button");
    }

    pub fn set_offline(&self, offline: bool, popup: bool) {
        let imp = imp::ArticleListColumn::from_instance(self);

        imp.mark_all_read_button.set_sensitive(!offline);
        imp.offline_button.set_visible(offline);
        imp.update_stack.set_visible(!offline);

        if popup {
            if offline {
                imp.offline_popover.read().as_ref().map(|pop| pop.popup());
                imp.online_popover.read().as_ref().map(|pop| pop.popdown());
            } else {
                imp.offline_popover.read().as_ref().map(|pop| pop.popdown());

                // workaround: popup slightly after parent button is shown
                glib::timeout_add_local(
                    Duration::from_millis(20),
                    clone!(@weak self as this => @default-panic, move || {
                        let imp = imp::ArticleListColumn::from_instance(&this);
                        imp.online_popover.read().as_ref().map(|pop| pop.popup());
                        Continue(false)
                    }),
                );
            }
        }
    }

    pub fn is_search_focused(&self) -> bool {
        let imp = imp::ArticleListColumn::from_instance(self);
        imp.search_button.is_active() && imp.search_entry.has_focus()
    }

    pub fn focus_search(&self) {
        let imp = imp::ArticleListColumn::from_instance(self);
        // shortcuts ignored when focues -> no need to hide seach bar on keybind (ESC still works)
        imp.search_button.set_active(true);
        imp.search_entry.grab_focus();
    }
}
