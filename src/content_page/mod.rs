mod article_list_column;
mod article_list_mode;
mod articleview_column;
mod content_page_state;
mod error;
mod offline_popover;
mod online_popover;
mod sidebar_column;

pub use self::article_list_column::ArticleListColumn;
pub use self::article_list_mode::ArticleListMode;
pub use self::articleview_column::ArticleViewColumn;
pub use self::content_page_state::ContentPageState;
pub use self::offline_popover::OfflinePopover;
pub use self::online_popover::OnlinePopover;
pub use self::sidebar_column::SidebarColumn;

use self::error::{ContentPageError, ContentPageErrorKind};
use crate::app::{Action, App};
use crate::article_list::ArticleListModel;
use crate::i18n::i18n_f;
use crate::responsive::ResponsiveLayout;
use crate::settings::Settings;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::{FeedListItemID, FeedListTree, TagListModel};
use crate::undo_action::UndoAction;
use crate::util::{Util, CHANNEL_ERROR};
use chrono::{Duration, NaiveDateTime, TimeZone, Utc};
use failure::ResultExt;
use futures::channel::oneshot;
use futures_util::future::FutureExt;
use glib::{clone, object::Cast, subclass};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{CompositeTemplate, Separator, Widget};
use libadwaita::{Leaflet, Toast, ToastOverlay};
use log::warn;
use news_flash::models::{
    Article, ArticleFilter, ArticleOrder, Category, CategoryType, Marked, PluginCapabilities, PluginID, Read,
    NEWSFLASH_TOPLEVEL,
};
use news_flash::{NewsFlash, NewsFlashError};
use parking_lot::RwLock;
use std::collections::HashSet;
use std::sync::Arc;

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/content_page.ui")]
    pub struct ContentPage {
        #[template_child]
        pub content_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub major_leaflet: TemplateChild<Leaflet>,
        #[template_child]
        pub minor_leaflet: TemplateChild<Leaflet>,
        #[template_child]
        pub major_separator: TemplateChild<Separator>,
        #[template_child]
        pub article_list_column: TemplateChild<ArticleListColumn>,
        #[template_child]
        pub sidebar_column: TemplateChild<SidebarColumn>,
        #[template_child]
        pub articleview_column: TemplateChild<ArticleViewColumn>,

        pub prev_settings: RwLock<Option<Settings>>,
        pub state: Arc<RwLock<ContentPageState>>,
        pub prev_state: Arc<RwLock<ContentPageState>>,

        pub toasts: Arc<RwLock<HashSet<Toast>>>,
        pub current_undo_action: Arc<RwLock<Option<UndoAction>>>,
        pub processing_undo_actions: Arc<RwLock<HashSet<UndoAction>>>,
        pub responsive_layout: Arc<RwLock<Option<ResponsiveLayout>>>,
    }

    impl Default for ContentPage {
        fn default() -> Self {
            Self {
                content_overlay: TemplateChild::default(),
                major_leaflet: TemplateChild::default(),
                minor_leaflet: TemplateChild::default(),
                major_separator: TemplateChild::default(),
                article_list_column: TemplateChild::default(),
                sidebar_column: TemplateChild::default(),
                articleview_column: TemplateChild::default(),

                prev_settings: RwLock::new(None),
                state: Arc::new(RwLock::new(ContentPageState::new())),
                prev_state: Arc::new(RwLock::new(ContentPageState::new())),

                toasts: Arc::new(RwLock::new(HashSet::new())),
                current_undo_action: Arc::new(RwLock::new(None)),
                processing_undo_actions: Arc::new(RwLock::new(HashSet::new())),
                responsive_layout: Arc::new(RwLock::new(None)),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContentPage {
        const NAME: &'static str = "ContentPage";
        type ParentType = gtk4::Box;
        type Type = super::ContentPage;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ContentPage {}

    impl WidgetImpl for ContentPage {}

    impl BoxImpl for ContentPage {}
}

glib::wrapper! {
    pub struct ContentPage(ObjectSubclass<imp::ContentPage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl ContentPage {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let imp = imp::ContentPage::from_instance(self);

        // workaround
        imp.minor_leaflet.set_hexpand(false);

        imp.sidebar_column.init();
        imp.article_list_column.init();
        imp.articleview_column.init();

        imp.minor_leaflet.set_visible_child(&*imp.sidebar_column);

        imp.responsive_layout.write().replace(ResponsiveLayout::new(self));

        imp.prev_settings
            .write()
            .replace(App::default().settings().read().clone());
    }

    pub fn state(&self) -> Arc<RwLock<ContentPageState>> {
        let imp = imp::ContentPage::from_instance(self);
        imp.state.clone()
    }

    pub fn prev_state(&self) -> Arc<RwLock<ContentPageState>> {
        let imp = imp::ContentPage::from_instance(self);
        imp.prev_state.clone()
    }

    pub fn simple_error(&self, message: &str) {
        let imp = imp::ContentPage::from_instance(self);
        let toast = Toast::new(message);
        toast.connect_dismissed(clone!(@weak self as this => @default-panic, move |toast| {
            let imp = imp::ContentPage::from_instance(&this);
            imp.toasts.write().remove(toast);
        }));
        imp.content_overlay.add_toast(&toast);
        imp.toasts.write().insert(toast);
    }

    pub fn newsflash_error(&self, message: &str, error: NewsFlashError) {
        let imp = imp::ContentPage::from_instance(self);
        let toast = Toast::new(message);
        toast.set_button_label(Some("details"));
        App::default().set_newsflash_error(error);
        toast.set_action_name(Some("win.show-error-dialog"));
        toast.connect_dismissed(clone!(@weak self as this => @default-panic, move |toast| {
            let imp = imp::ContentPage::from_instance(&this);
            imp.toasts.write().remove(toast);
        }));
        imp.content_overlay.add_toast(&toast);
        imp.toasts.write().insert(toast);
    }

    pub fn dismiss_notifications(&self) {
        let imp = imp::ContentPage::from_instance(self);

        for toast in imp.toasts.write().drain() {
            toast.dismiss();
        }
    }

    pub fn remove_current_undo_action(&self) {
        let imp = imp::ContentPage::from_instance(self);
        let _ = imp.current_undo_action.write().take();

        // update lists
        Util::send(Action::UpdateSidebar);
        Util::send(Action::UpdateArticleList);
    }

    pub fn add_undo_notification(&self, action: UndoAction) {
        let imp = imp::ContentPage::from_instance(self);

        if let Some(current_action) = imp.current_undo_action.write().take() {
            log::debug!("remove current action: {}", current_action);
            Self::execute_action(&current_action, &imp.processing_undo_actions);

            // dismiss all other toasts
            for toast in imp.toasts.write().drain() {
                toast.dismiss();
            }
        }

        let message = match &action {
            UndoAction::DeleteCategory(_id, label) => i18n_f("Deleted Category '{}'", &[label]),
            UndoAction::DeleteFeed(_id, label) => i18n_f("Deleted Feed '{}'", &[label]),
            UndoAction::DeleteTag(_id, label) => i18n_f("Deleted Tag '{}'", &[label]),
        };

        let toast = Toast::new(&message);
        toast.set_button_label(Some("undo"));
        toast.set_action_name(Some("win.remove-undo-action"));
        toast.connect_dismissed(clone!(
            @strong action,
            @weak self as this => @default-panic, move |toast| {
                let imp = imp::ContentPage::from_instance(&this);

                // if this is locked we're dismissing all toasts anyway
                if imp.current_undo_action.is_locked_exclusive() {
                    return;
                }

                if let Some(current_action) = imp.current_undo_action.write().take() {
                    log::debug!("remove current action: {}", current_action);
                    Self::execute_action(&current_action, &imp.processing_undo_actions);
                };

                imp.toasts.write().remove(toast);
        }));
        imp.content_overlay.add_toast(&toast);
        imp.toasts.write().insert(toast);

        imp.current_undo_action.write().replace(action);

        // update lists
        Util::send(Action::UpdateSidebar);
        Util::send(Action::UpdateArticleList);
    }

    pub fn execute_pending_undoable_action(&self) {
        let imp = imp::ContentPage::from_instance(self);

        if let Some(current_action) = self.get_current_undo_action() {
            Self::execute_action(&current_action, &imp.processing_undo_actions);
        }
    }

    pub fn get_current_undo_action(&self) -> Option<UndoAction> {
        let imp = imp::ContentPage::from_instance(self);
        imp.current_undo_action.read().as_ref().map(|a| a.clone())
    }

    pub fn processing_undo_actions(&self) -> Arc<RwLock<HashSet<UndoAction>>> {
        let imp = imp::ContentPage::from_instance(self);
        imp.processing_undo_actions.clone()
    }

    fn execute_action(action: &UndoAction, processing_actions: &Arc<RwLock<HashSet<UndoAction>>>) {
        processing_actions.write().insert(action.clone());
        let callback = Box::new(
            clone!(@strong processing_actions, @strong action => @default-panic, move || {
                processing_actions.write().remove(&action);
            }),
        );
        match action {
            UndoAction::DeleteFeed(feed_id, _label) => {
                Util::send(Action::DeleteFeed(feed_id.clone(), callback));
            }
            UndoAction::DeleteCategory(category_id, _label) => {
                Util::send(Action::DeleteCategory(category_id.clone(), callback));
            }
            UndoAction::DeleteTag(tag_id, _label) => {
                Util::send(Action::DeleteTag(tag_id.clone(), callback));
            }
        }
    }

    pub fn sidebar_column(&self) -> &SidebarColumn {
        let imp = imp::ContentPage::from_instance(self);
        &imp.sidebar_column
    }

    pub fn article_list_column(&self) -> &ArticleListColumn {
        let imp = imp::ContentPage::from_instance(self);
        &imp.article_list_column
    }

    pub fn articleview_column(&self) -> &ArticleViewColumn {
        let imp = imp::ContentPage::from_instance(self);
        &imp.articleview_column
    }

    pub fn major_leaflet(&self) -> &Leaflet {
        let imp = imp::ContentPage::from_instance(self);
        &imp.major_leaflet
    }

    pub fn minor_leaflet(&self) -> &Leaflet {
        let imp = imp::ContentPage::from_instance(self);
        &imp.minor_leaflet
    }

    pub fn responsive_layout(&self) -> ResponsiveLayout {
        let imp = imp::ContentPage::from_instance(self);
        imp.responsive_layout
            .read()
            .clone()
            .expect("ContentPage not initialized")
    }

    pub fn load_branding(&self) -> bool {
        if let Some(id) = App::default().news_flash().read().as_ref().and_then(|n| n.id()) {
            let user_name = App::default().news_flash().read().as_ref().and_then(|n| n.user_name());
            if self.load_branding_impl(&id, user_name.as_deref()) {
                // try to fill content page with data
                self.update_sidebar();
                self.update_article_list();
            }
            true
        } else {
            false
        }
    }

    fn load_branding_impl(&self, id: &PluginID, user_name: Option<&str>) -> bool {
        if self.sidebar_column().set_account(id, user_name).is_err() {
            Util::send(Action::ErrorSimpleMessage("Failed to set service logo.".to_owned()));
            false
        } else {
            true
        }
    }

    pub fn clear(&self) {
        let imp = imp::ContentPage::from_instance(self);
        let settings = App::default().settings();
        self.articleview_column().article_view().close_article();
        self.state().write().set_prefer_scraped_content(false);

        let empty_list_model = ArticleListModel::new(&settings.read().get_article_list_order());
        self.article_list_column().article_list().update(
            empty_list_model,
            &self.state(),
            true,
            &self.prev_state().read().clone(),
        );

        let feed_tree_model = FeedListTree::new();
        self.sidebar_column().sidebar().update_feedlist(feed_tree_model);

        let tag_list_model = TagListModel::new();
        self.sidebar_column().sidebar().update_taglist(tag_list_model);
        self.sidebar_column().sidebar().hide_taglist();
        imp.prev_settings.write().replace(settings.read().clone());
        *self.prev_state().write() = self.state().read().clone();
    }

    pub fn update_article_view_background(&self) {
        if let Some(color) = self.article_list_column().article_list().get_background_color() {
            self.articleview_column().article_view().update_background_color(&color);
        }
    }

    pub fn update_article_list(&self) {
        let imp = imp::ContentPage::from_instance(self);
        let settings = App::default().settings();
        let (sender, receiver) = oneshot::channel::<Result<ArticleListModel, ContentPageErrorKind>>();

        let last_article_in_list_date = self
            .article_list_column()
            .article_list()
            .get_last_row_model()
            .map(|model| model.date);
        // Is this a new list based on changed persitent settings?
        let new_list_because_of_settings = if let Some(prev_settings) = imp.prev_settings.read().as_ref() {
            settings.read().get_article_list_order() != prev_settings.get_article_list_order()
                || settings.read().get_article_list_show_thumbs() != prev_settings.get_article_list_show_thumbs()
        } else {
            false
        };
        // Is this a new list based on changed window state? (e.g. different sidebar selection)
        let new_list_because_of_window_state = *self.state().read() != *self.prev_state().read();

        let is_new_list = new_list_because_of_settings || new_list_because_of_window_state;
        let prev_state = self.prev_state().read().clone();

        let window_state = self.state().clone();
        let current_undo_action = self.get_current_undo_action();
        let processing_undo_actions = imp.processing_undo_actions.clone();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let (limit, last_article_date) = if is_new_list {
                    (Some(ContentPageState::page_size()), None)
                } else {
                    if last_article_in_list_date.is_none() {
                        // article list is empty: load default amount of articles to fill it
                        (Some(ContentPageState::page_size()), None)
                    } else {
                        (None, last_article_in_list_date)
                    }
                };

                let mut list_model = ArticleListModel::new(&settings.read().get_article_list_order());
                let mut articles = match Self::load_articles(
                    news_flash,
                    &window_state,
                    &current_undo_action,
                    &processing_undo_actions,
                    limit,
                    None,
                    last_article_date,
                    settings.read().get_article_list_hide_future_articles(),
                ) {
                    Ok(articles) => articles,
                    Err(error) => {
                        sender.send(Err(error.kind())).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                let loaded_article_count = articles.len() as i64;
                if loaded_article_count < ContentPageState::page_size() {
                    // article list is not filled all the way up to "page size"
                    // load a few more
                    let mut more_articles = match Self::load_articles(
                        news_flash,
                        &window_state,
                        &current_undo_action,
                        &processing_undo_actions,
                        Some(ContentPageState::page_size() - loaded_article_count),
                        Some(loaded_article_count),
                        None,
                        settings.read().get_article_list_hide_future_articles(),
                    ) {
                        Ok(articles) => articles,
                        Err(_error) => {
                            log::warn!("loading more articles to fill up to page size failed");
                            Vec::new()
                        }
                    };
                    articles.append(&mut more_articles);
                }

                let feeds = match news_flash.get_feeds() {
                    Ok((feeds, _)) => feeds,
                    Err(_error) => {
                        sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let articles = articles
                    .drain(..)
                    .map(|article| {
                        let feed = feeds.iter().find(|f| f.feed_id == article.feed_id);
                        (article, feed)
                    })
                    .collect();

                list_model.add(articles);

                sender.send(Ok(list_model)).expect(CHANNEL_ERROR);
            }
        };

        let article_list = self.article_list_column().article_list();
        let window_state = self.state();
        let glib_future = receiver.map(clone!(@weak article_list => @default-panic, move |res| {
            if let Ok(res) = res {
                if let Ok(article_list_model) = res {
                    article_list
                        .update(article_list_model, &window_state, is_new_list, &prev_state);
                }
            }
        }));

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
        imp.prev_settings
            .write()
            .replace(App::default().settings().read().clone());
        *self.prev_state().write() = self.state().read().clone();
    }

    pub fn load_more_articles(&self) {
        let imp = imp::ContentPage::from_instance(self);
        let (sender, receiver) = oneshot::channel::<Result<ArticleListModel, ContentPageErrorKind>>();

        let relevant_articles_loaded = self
            .article_list_column()
            .article_list()
            .get_relevant_article_count(self.state().read().get_article_list_mode());

        let current_undo_action = self.get_current_undo_action();
        let processing_undo_actions = imp.processing_undo_actions.clone();
        let window_state = self.state().clone();
        let thread_future = async move {
            let mut list_model = ArticleListModel::new(&App::default().settings().read().get_article_list_order());

            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let mut articles = match Self::load_articles(
                    news_flash,
                    &window_state,
                    &current_undo_action,
                    &processing_undo_actions,
                    Some(ContentPageState::page_size()),
                    Some(relevant_articles_loaded as i64),
                    None,
                    App::default().settings().read().get_article_list_hide_future_articles(),
                ) {
                    Ok(articles) => articles,
                    Err(error) => {
                        sender.send(Err(error.kind())).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let feeds = match news_flash.get_feeds() {
                    Ok((feeds, _)) => feeds,
                    Err(_error) => {
                        sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let articles = articles
                    .drain(..)
                    .map(|article| {
                        let feed = feeds.iter().find(|f| f.feed_id == article.feed_id);
                        (article, feed)
                    })
                    .collect();

                list_model.add(articles);

                sender.send(Ok(list_model)).expect(CHANNEL_ERROR);
            }
        };

        let article_list = self.article_list_column().article_list().clone();
        let glib_future = receiver.map(move |res| {
            if let Ok(res) = res {
                if let Ok(article_list_model) = res {
                    article_list.add_more_articles(article_list_model);
                }
            }
        });

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn load_articles(
        news_flash: &NewsFlash,
        window_state: &RwLock<ContentPageState>,
        current_undo_action: &Option<UndoAction>,
        processing_undo_actions: &Arc<RwLock<HashSet<UndoAction>>>,
        limit: Option<i64>,
        offset: Option<i64>,
        last_article_date: Option<NaiveDateTime>,
        hide_future_articles: bool,
    ) -> Result<Vec<Article>, ContentPageError> {
        let settings = App::default().settings();
        let limit = match limit {
            Some(limit) => Some(limit),
            None => Some(i64::MAX),
        };
        let unread = match window_state.read().get_article_list_mode() {
            ArticleListMode::All | ArticleListMode::Marked => None,
            ArticleListMode::Unread => Some(Read::Unread),
        };
        let marked = match window_state.read().get_article_list_mode() {
            ArticleListMode::All | ArticleListMode::Unread => None,
            ArticleListMode::Marked => Some(Marked::Marked),
        };
        let feed = match &window_state.read().get_sidebar_selection() {
            SidebarSelection::All | SidebarSelection::Tag(_, _) => None,
            SidebarSelection::FeedList(id, _title) => match &**id {
                FeedListItemID::Feed(id, _) => Some(id.clone()),
                _ => None,
            },
        };
        let category = match &window_state.read().get_sidebar_selection() {
            SidebarSelection::All | SidebarSelection::Tag(_, _) => None,
            SidebarSelection::FeedList(id, _title) => match &**id {
                FeedListItemID::Category(id) => Some(id.clone()),
                _ => None,
            },
        };
        let tag = match &window_state.read().get_sidebar_selection() {
            SidebarSelection::All | SidebarSelection::FeedList(..) => None,
            SidebarSelection::Tag(id, _title) => Some((**id).clone()),
        };
        let (older_than, newer_than) = if let Some(last_article_date) = last_article_date {
            let order = settings.read().get_article_list_order();
            match order {
                // +/- 1s to not exclude last article in list
                ArticleOrder::NewestFirst => (
                    None,
                    Some(Utc.from_utc_datetime(&last_article_date) - Duration::seconds(1)),
                ),
                ArticleOrder::OldestFirst => (
                    Some(Utc.from_utc_datetime(&last_article_date) + Duration::seconds(1)),
                    None,
                ),
            }
        } else {
            (None, None)
        };

        // mutate older_than to hide articles in the future
        let older_than = if hide_future_articles {
            if let Some(older_than) = older_than {
                if older_than < Utc::now() {
                    Some(older_than)
                } else {
                    Some(Utc::now())
                }
            } else {
                Some(Utc::now())
            }
        } else {
            older_than
        };

        let search_term = window_state.read().get_search_term().clone();
        let (mut feed_blacklist, category_blacklist) = {
            let mut undo_actions = Vec::new();
            let mut feed_blacklist = Vec::new();
            let mut category_blacklist = Vec::new();
            if let Some(current_undo_action) = current_undo_action {
                undo_actions.push(current_undo_action);
            }
            let processing_undo_actions = &*processing_undo_actions.read();
            for processing_undo_action in processing_undo_actions {
                undo_actions.push(&processing_undo_action);
            }
            for undo_action in undo_actions {
                match undo_action {
                    UndoAction::DeleteFeed(feed_id, _label) => feed_blacklist.push(feed_id.clone()),
                    UndoAction::DeleteCategory(category_id, _label) => category_blacklist.push(category_id.clone()),
                    UndoAction::DeleteTag(_tag_id, _label) => {}
                }
            }

            let feed_blacklist = if feed_blacklist.is_empty() {
                None
            } else {
                Some(feed_blacklist)
            };
            let category_blacklist = if category_blacklist.is_empty() {
                None
            } else {
                Some(category_blacklist)
            };

            (feed_blacklist, category_blacklist)
        };

        // if selected category is special category "uncategorized" set category filter to `None`
        // and add all feeds to the feed blacklist
        let category = if category == Some(crate::util::NEWSFLASH_UNCATEGORIZED.clone()) {
            let (_feeds, mappings) = match news_flash.get_feeds() {
                Ok(res) => res,
                Err(_error) => return Err(ContentPageErrorKind::DataBase.into()),
            };

            feed_blacklist = if mappings.is_empty() {
                None
            } else {
                if let Some(mut blacklist) = feed_blacklist {
                    for mapping in mappings {
                        blacklist.push(mapping.feed_id.clone());
                    }
                    Some(blacklist)
                } else {
                    let mut blacklist = Vec::new();
                    for mapping in mappings {
                        blacklist.push(mapping.feed_id.clone());
                    }
                    Some(blacklist)
                }
            };

            None
        } else {
            category
        };

        let articles = news_flash
            .get_articles(ArticleFilter {
                limit,
                offset,
                order: Some(settings.read().get_article_list_order()),
                unread,
                marked,
                feed,
                feed_blacklist,
                category,
                category_blacklist,
                tag,
                ids: None,
                newer_than,
                older_than,
                search_term,
            })
            .context(ContentPageErrorKind::DataBase)?;

        Ok(articles)
    }

    pub fn update_sidebar(&self) {
        let imp = imp::ContentPage::from_instance(self);
        let (glib_sender, receiver) =
            oneshot::channel::<Result<(i64, FeedListTree, Option<TagListModel>), ContentPageErrorKind>>();

        let state = self.state().clone();
        let current_undo_action = self.get_current_undo_action();
        let processing_undo_actions = imp.processing_undo_actions.clone();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let mut tree = FeedListTree::new();
                let mut tag_list_model: Option<TagListModel> = None;

                let mut categories = match news_flash.get_categories() {
                    Ok(categories) => categories,
                    Err(_error) => {
                        glib_sender
                            .send(Err(ContentPageErrorKind::DataBase))
                            .expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let (feeds, mut mappings) = match news_flash.get_feeds() {
                    Ok(res) => res,
                    Err(_error) => {
                        glib_sender
                            .send(Err(ContentPageErrorKind::DataBase))
                            .expect(CHANNEL_ERROR);
                        return;
                    }
                };

                // collect unread and marked counts
                let hide_future_articles = App::default().settings().read().get_article_list_hide_future_articles();
                let feed_count_map = match state.read().get_article_list_mode() {
                    ArticleListMode::All | ArticleListMode::Unread => {
                        match news_flash.unread_count_feed_map(hide_future_articles) {
                            Ok(res) => res,
                            Err(_error) => {
                                glib_sender
                                    .send(Err(ContentPageErrorKind::DataBase))
                                    .expect(CHANNEL_ERROR);
                                return;
                            }
                        }
                    }
                    ArticleListMode::Marked => match news_flash.marked_count_feed_map() {
                        Ok(res) => res,
                        Err(_error) => {
                            glib_sender
                                .send(Err(ContentPageErrorKind::DataBase))
                                .expect(CHANNEL_ERROR);
                            return;
                        }
                    },
                };

                let mut pending_delte_feeds = HashSet::new();
                let mut pending_delete_categories = HashSet::new();
                let mut pending_delete_tags = HashSet::new();
                if let Some(current_undo_action) = &current_undo_action {
                    match current_undo_action {
                        UndoAction::DeleteFeed(id, _label) => pending_delte_feeds.insert(id),
                        UndoAction::DeleteCategory(id, _label) => pending_delete_categories.insert(id),
                        UndoAction::DeleteTag(id, _label) => pending_delete_tags.insert(id),
                    };
                }
                let processing_undo_actions = &*processing_undo_actions.read();
                for processing_undo_action in processing_undo_actions {
                    match processing_undo_action {
                        UndoAction::DeleteFeed(id, _label) => pending_delte_feeds.insert(&id),
                        UndoAction::DeleteCategory(id, _label) => pending_delete_categories.insert(&id),
                        UndoAction::DeleteTag(id, _label) => pending_delete_tags.insert(&id),
                    };
                }

                // If there are feeds without a category:
                // - create new 'generated' category "Uncategorized"
                // - create mappings for all feeds without category to be children of the newly generated category
                let mut uncategorized_mappings = Util::create_mappings_for_uncategorized_feeds(&feeds, &mappings);
                if !uncategorized_mappings.is_empty() {
                    categories.push(Category {
                        category_id: crate::util::NEWSFLASH_UNCATEGORIZED.clone(),
                        label: "Uncategorized".into(),
                        sort_index: None,
                        parent_id: NEWSFLASH_TOPLEVEL.clone(),
                        category_type: CategoryType::Generated,
                    });
                }
                mappings.append(&mut uncategorized_mappings);

                // feedlist: Categories
                for category in &categories {
                    if pending_delete_categories.contains(&category.category_id) {
                        continue;
                    }

                    let category_item_count = Util::calculate_item_count_for_category(
                        &category.category_id,
                        &categories,
                        &mappings,
                        &feed_count_map,
                        &pending_delte_feeds,
                        &pending_delete_categories,
                    );

                    if tree.add_category(category, category_item_count).is_err() {
                        glib_sender
                            .send(Err(ContentPageErrorKind::SidebarModels))
                            .expect(CHANNEL_ERROR);
                        return;
                    }
                }

                // feedlist: Feeds
                for mapping in &mappings {
                    if pending_delte_feeds.contains(&mapping.feed_id)
                        || pending_delete_categories.contains(&mapping.category_id)
                    {
                        continue;
                    }

                    let feed = match feeds.iter().find(|feed| feed.feed_id == mapping.feed_id) {
                        Some(res) => res,
                        None => {
                            warn!(
                                "Mapping for feed '{}' exists, but can't find the feed itself",
                                mapping.feed_id
                            );
                            Util::send(Action::ErrorSimpleMessage(format!(
                                "Sidebar: missing feed with id: '{}'",
                                mapping.feed_id
                            )));
                            continue;
                        }
                    };

                    let item_count = match feed_count_map.get(&mapping.feed_id) {
                        Some(count) => *count,
                        None => 0,
                    };
                    if tree.add_feed(&feed, &mapping, item_count).is_err() {
                        //sender.send(Err(ContentPageErrorKind::SidebarModels)).expect(CHANNEL_ERROR);
                    }
                }

                // tag list
                let support_tags = App::default()
                    .features()
                    .read()
                    .contains(PluginCapabilities::SUPPORT_TAGS);

                if support_tags {
                    let mut list = TagListModel::new();
                    let tags = match news_flash.get_tags() {
                        Ok(tags) => tags,
                        Err(_error) => {
                            glib_sender
                                .send(Err(ContentPageErrorKind::DataBase))
                                .expect(CHANNEL_ERROR);
                            return;
                        }
                    };

                    if !tags.is_empty() {
                        for tag in tags {
                            if pending_delete_tags.contains(&tag.tag_id) {
                                continue;
                            }
                            if list.add(&tag).is_err() {
                                glib_sender
                                    .send(Err(ContentPageErrorKind::SidebarModels))
                                    .expect(CHANNEL_ERROR);
                                return;
                            }
                        }
                    }

                    tag_list_model = Some(list);
                }

                //let total_item_count = feed_count_map.iter().map(|(_key, value)| value).sum();
                let total_item_count = Util::calculate_item_count_for_category(
                    &NEWSFLASH_TOPLEVEL,
                    &categories,
                    &mappings,
                    &feed_count_map,
                    &pending_delte_feeds,
                    &pending_delete_categories,
                );

                glib_sender
                    .send(Ok((total_item_count, tree, tag_list_model)))
                    .expect(CHANNEL_ERROR);
            }
        };

        let sidebar = self.sidebar_column().sidebar().clone();
        let glib_future = receiver.map(move |res| match res {
            Ok(res) => match res {
                Ok((total_count, feed_list_model, tag_list_model)) => {
                    sidebar.update_feedlist(feed_list_model);
                    sidebar.update_all(total_count);
                    if let Some(tag_list_model) = tag_list_model {
                        if tag_list_model.is_empty() {
                            sidebar.hide_taglist();
                        } else {
                            sidebar.update_taglist(tag_list_model);
                            sidebar.show_taglist();
                        }
                    } else {
                        sidebar.hide_taglist();
                    }
                }
                Err(error) => Util::send(Action::ErrorSimpleMessage(format!(
                    "Failed to update sidebar: '{}'",
                    error
                ))),
            },
            Err(error) => Util::send(Action::ErrorSimpleMessage(format!(
                "Failed to update sidebar: '{}'",
                error
            ))),
        });

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
        imp.prev_settings
            .write()
            .replace(App::default().settings().read().clone());
        *self.prev_state().write() = self.state().read().clone();
    }

    pub fn article_view_scroll_diff(&self, diff: f64) -> Result<(), ContentPageError> {
        self.articleview_column()
            .article_view()
            .animate_scroll_diff(diff)
            .context(ContentPageErrorKind::ArticleView)?;
        Ok(())
    }

    pub fn sidebar_select_next_item(&self) {
        self.sidebar_column().sidebar().select_next_item()
    }

    pub fn sidebar_select_prev_item(&self) {
        self.sidebar_column().sidebar().select_prev_item()
    }

    pub fn start_sync(&self) {
        self.sidebar_column().start_sync();
        self.article_list_column().start_sync();
    }

    pub fn finish_sync(&self) {
        self.sidebar_column().finish_sync();
        self.article_list_column().finish_sync();
    }

    pub fn set_offline(&self, offline: bool) {
        let show_sidebar_offline_popover = self.show_sidebar_offline_popover();

        self.state().write().set_offline(offline);
        self.articleview_column().set_offline(offline);
        self.article_list_column()
            .set_offline(offline, !show_sidebar_offline_popover);
        self.sidebar_column().set_offline(offline, show_sidebar_offline_popover);
    }

    fn show_sidebar_offline_popover(&self) -> bool {
        let imp = imp::ContentPage::from_instance(self);

        if !imp.minor_leaflet.is_folded() {
            return false;
        }

        imp.minor_leaflet.visible_child() == Some(imp.sidebar_column.clone().upcast::<Widget>())
    }

    pub fn enter_video_fullscreen(&self) {
        let imp = imp::ContentPage::from_instance(self);
        imp.articleview_column.headerbar().hide();
        imp.minor_leaflet.hide();
        imp.major_separator.hide();
    }

    pub fn leave_video_fullscreen(&self) {
        let imp = imp::ContentPage::from_instance(self);
        imp.articleview_column.headerbar().show();
        imp.minor_leaflet.show();
        imp.major_separator.show();
    }
}
